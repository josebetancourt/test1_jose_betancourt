package tests;
import car.Car;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void testNegativeSpeed() {
		try {
			Car car1 = new Car(-1);
			fail("The Car constructor should throw an exception but it didn't");
		}
		catch (IllegalArgumentException e){
			
		}
		catch (Exception e) {
			fail("The Car constructor thrwo a different exception than supposed");
		}
	}
	
	@Test
	void testGets() {
		Car car1 = new Car(10);
		assertEquals(10, car1.getSpeed());
		assertEquals(0, car1.getLocation());
	}
	
	@Test
	void testMoveRight() {
		Car car1 = new Car(50);
		car1.moveRight();
		assertEquals(50, car1.getLocation());
	}
	
	@Test
	void testMoveLeft() {
		Car car1 = new Car(10);
		car1.moveLeft();
		assertEquals(-10, car1.getLocation());
	}
	
	@Test
	void testAccelerate() {
		Car car1 = new Car(10);
		car1.accelerate();
		assertEquals(11, car1.getSpeed());
	}
	
	@Test
	void testDecelerate() {
		Car car1 = new Car(10);
		car1.decelerate();
		assertEquals(9, car1.getSpeed());
	}
	
	@Test
	void testDecelerateStopped() {
		Car car1 = new Car(0);
		car1.decelerate();
		assertEquals(0, car1.getSpeed());
	}

}
