package utilities;

//import java.util.Arrays;

public class MatrixMethod {

    public static int[][] duplicate(int[][] arr) {
        int[][] newArr = new int[arr.length][arr[0].length*2];
        for (int i=0; i < arr.length; i++) {
        	int k = 0;
            for (int j=0; j < arr[i].length; j++) {
                newArr[i][k] = arr[i][j];
                k++;
                newArr[i][k] = arr[i][j];
                k++;
            }
        }
        return newArr;
    }
    
//	  To print the arrays and see the content, 
//    i couldn't duplicate the array in the same way as in the example
//    instead of 1,2,3,1,2,3 i get 1,1,2,2,3,3
//    
//    public static void main(String[] args) {
//    	int [][] arr = {  { 1,2,3} , {4,5,6 } } ;
//    	int[][] newArr = duplicate(arr);
//        for (int k=0; k< newArr.length;k++) {
//        	System.out.println(Arrays.toString(newArr[k]));
//        }
//    }
}
