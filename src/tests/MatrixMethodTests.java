package tests;
import utilities.MatrixMethod;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void testDuplicate() {
		int [][] arr1 = {  { 1,2,3} , {4,5,6 } };
		int [][] arr1Duplicated = MatrixMethod.duplicate(arr1);
		assertArrayEquals(new int[][] { { 1,1,2,2,3,3} , {4,4,5,5,6,6} }, arr1Duplicated);
		
	}

}
